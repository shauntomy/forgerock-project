// Importing required modules
require('dotenv').config()
const express = require("express")
const axios = require('axios');
const app = express()


/**
* This function is used to retreive the data from Alphavantage API and process it
* @return   {Object}         returns an object that contains average closing price over nDays and all the stock data across nDays
*/
let retrieveData = async () => {
    try {
        console.log(process.env['API_KEY'])
        const response = await axios.get(`https://www.alphavantage.co/query?apikey=${process.env['API_KEY']}&function=TIME_SERIES_DAILY_ADJUSTED&symbol=${process.env['SYMBOL']}`)
        let nDays = process.env['NDAYS']
        let processedData = processDataByDate(response.data['Time Series (Daily)'], nDays)
        const averageClosingPrice = findAverage(processedData, nDays)

        const returnData = {
            'Average closing price': averageClosingPrice,
            'data': processedData
        }
        return returnData;
    } catch (err) {
        throw err
    }

};

/**
* Function returns only data in last nDays of stock trading
* @param    {String} stockData      The stock data returned by Alphavantage API
* @param    {number} nDays          How many days of past data is required
* @return   {Object}                Returns only data over last nDays
*/
let processDataByDate = (stockData, nDays) => {
    let nDaysData = {}
    for (let i = 0; i < nDays; i++) {
        nDaysData[Object.keys(stockData)[i]] = Object.values(stockData)[i]
    }
    return nDaysData
}


/**
* Function finds the average closing price over the last nDays
* @param    {String} data           The processed stock data by proceessDatabyDate() function
* @param    {number} nDays          How many days of past data is required
* @return   {number}                Returns average closing price over nDays
*/
let findAverage = (data, nDays) => {
    let sum = 0
    for (const value of Object.values(data)) {
        sum = sum + parseFloat(value['4. close'])
    }
    return (sum / nDays).toFixed(2)
}


// Handling GET / request
app.get("/", async (req, res, next) => {
    res.setHeader('Content-Type', 'application/json')
    let responseData = await retrieveData(5)
    console.log(responseData)
    res.send(responseData)
})

// Server setup
app.listen(3000, () => {
    console.log("Server is Running")
})