# Part 1: Stock Ticker

For this task, I have chosen to use nodejs due to the limited time I had to learn Go. I attempted using GoLang however it requires me to have proper understanding of how structs are used in that language. Therefore, I ended up using nodejs.

## How can I run the server locally
1. Ensure you have node installed on your machine. Follow this [link](https://nodejs.org/en/download/) to install node.
2. run `npm install` to install necessary packages.
3. Run `npm run` to start the server.

The following variables in code is passed in as env variables:
- API_KEY (api key of alphavantage)
- SYMBOL (the stock symbol)
- NDAYS (the last NDAYS of stock data)

I have used a .env file to pass in the env variables. Here are the contents of the .env file:

```
# API KEY
API_KEY=<YOUR API KEY>

# SYMBOL
SYMBOL=MSFT

# NDAYS
NDAYS=7
```

## Where's the entrypoint?
The entrypoint for this repository is the `index.js` file. I have added comments throughout the code to explain what the various functions do.

## How can I build and run docker image locally?
Firstly, please ensure that you have Docker installed. You can install it from [this link](https://docs.docker.com/get-docker/). Once you have Docker installed follow the below instructions

### 1. Build the image
To build the image, please run the below command:
```
docker build . -t forgerock-app 
```
You can tag your image using `-t` as it will make it easier to find the Docker image.

Run `docker images` to see the image that you have built.

### 2. Run the docker container
```
docker run -p 49160:3000 forgerock-app  
```
This command will run the docker container. You would be able to access the docker container from your browser by going on `http://localhost:49160`.

`-p` is used to redirect your device port to a private port inside the container. For this example, the container is exposed on port 49160 on your machine.

# Part 2: Kubernetes
In this section, I will go through how I am deploying the container created in the previous step to Kubernetes. I will be deploying to a local Kubernetes cluster that's running using minikube.

## Setup minikube
Firstly, install minikube [here](https://minikube.sigs.k8s.io/docs/start/).

Then, run the following command to point your terminal's docker-cli to Docker Engine inside minikube:
```
eval $(minikube docker-env)
```

Build the docker image again to ensure that minikube would have access to the docker container:
```
docker build . -t forgerock-app 
```

I have setup my own kubernetes namespace for this exercise. You can create the namespace by running the below command:
```
kubectl create namespace stockapp
```

## Creating secrets
For this exercise, I am storing the API Key for Alphavantage as a Kubernetes secret and passing it through to the container as an env variable.

I had created a file called secret.yaml (this is not committed to git) and I deployed the yaml file by running the below command:

```
kubectl apply -f ./kubernetes/secret.yaml  
```

The contents of the secret file are:

```
apiVersion: v1
kind: Secret
metadata:  
  name: stockapp-secrets
  namespace: stockapp
type: Opaque
data:
  apikey:  <YOUR API KEY>
```

## Deploy the yaml files
Now we can apply the yaml files to deploy to the Kubernetes cluster.

Firstly lets deploy our app by applying deployment.yaml file:
```
kubectl apply -f ./kubernetes/deployment.yaml  
```

To ensure the pods are discoverable internally or externally, we need to ensure that we expose the deployment through a service. So let's deploy our stockapp-service.yaml:

```
kubectl apply -f ./kubernetes/stockapp-service.yaml  
```
Finally, let's deploy the ingress which sits one layer above the service we had deployed. Ingress is a load balancer which is used to expose our app to the external world

```
kubectl apply -f ./kubernetes/ingress.yaml
```

# Part 3: Resilience
In this section, I am going to lay out few things that can be done to make the Kubernetes cluster more resilient
- Ensure there multiple pods deployed in several nodes
- If using GCP, use options such as regional cluster instead of zonal cluster which would ensure the Kubernetes cluster is deployed in multiple zones